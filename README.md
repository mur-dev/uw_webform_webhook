## Description

This module adds an extra tab to each webform node, allowing you to specify
a webhook endpoint to submit form submissions to in the form of a JSON object via POST.

## Installation

1. Place the module folder in your sites/all/modules folder
2. Make sure you have the webform module enabled
3. Activate the module via admin/build/modules

## Usage

Once you have installed the module, you can find the webhook settings in the
Form Settings tab under Advanced Settings.
You can add a webhook endpoint URL to your webform and also enable
or disable the currently set webhook.

Every time a user fills in the webform, if enabled, the webhook will be triggered.
A JSON object with the form submission values is POSTed to the webhook URL endpoint.
Only JSON is supported at this time.

There are no user interactions, the request is performed behind the scenes.
