<?php

/**
 * @file
 * Manages webform webhooks administration UI.
 */

/**
 * Use hook_form_alter to add webhook options to advanced form settings.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form State.
 * @param string $form_id
 *   Form id.
 *
 * @return array
 *   Returns the modified form
 */
function uw_webform_webhook_form_webform_configure_form_alter(array &$form, array &$form_state, $form_id) {
  $node = $form['#node'];
  $webhook = uw_webform_webhook_get_node_webhook($node->nid);

  $form['advanced']['webhook'] = array(
    '#type' => 'fieldset',
    '#title' => t('Webhook Settings'),
    '#collapsible' => FALSE,
  );
  $form['advanced']['webhook']['webhook-url'] = [
    '#type' => 'textfield',
    '#maxlength' => 2048,
    '#title' => t('Webhook URL'),
    '#description' => t('The webhook URL endpoint to POST the json formatted form submission.  Use of HTTPS endpoints is highly recommended.'),
    '#default_value' => isset($webhook['url']) ? $webhook['url'] : '',
    '#element_validate' => ['uw_webform_webhook_manage_form_validate'],
  ];
  $form['advanced']['webhook']['webhook-enabled'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable Webhook'),
    '#description' => t('Check this box to enable your webhook functionality.'),
    '#default_value' => isset($webhook['enabled']) ? $webhook['enabled'] : FALSE,
  ];

  // Add webhook submit handler to form.
  $form['#submit'][] = 'uw_webform_webhook_manage_form_submit';

  return $form;
}

/**
 * Submit function for webhook manage form.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form State.
 */
function uw_webform_webhook_manage_form_submit(array $form, array &$form_state) {
  // Save the rule weights.
  $values = [
    'url' => $form_state['values']['webhook-url'],
    'enabled' => $form_state['values']['webhook-enabled'],
    'nid' => $form_state['values']['nid'],
  ];

  // If url was cleared, set the form to disabled.
  if (!(string) $values['url']) {
    $values['enabled'] = FALSE;
  }
  // Include nid as primary key if record exists, this updates vs inserts.
  $exists = uw_webform_webhook_get_node_webhook($values['nid']);
  drupal_write_record('uw_webform_webhook', $values, $exists ? 'nid' : []);
}

/**
 * Validation handler to add / edit the webhook.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form State.
 */
function uw_webform_webhook_manage_form_validate(array $form, array &$form_state) {
  $values = $form_state['values'];
  // Ensure we have a valid URL stored, checks for 0 string as well.
  if (strlen($values['webhook-url']) && !valid_url($values['webhook-url'], TRUE)) {
    form_set_error('url', t('Please enter a valid webhook endpoint URL.'));
  }
}
